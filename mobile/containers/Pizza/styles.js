import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
  header: {
    borderBottomWidth: 2,
    borderBottomColor: '#444',
    padding: 10,
    height: 40,
  },
  footer: {
    borderTopWidth: 2,
    borderTopColor: '#444',
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  footer_total: {
    flexGrow: 1,
    fontWeight: 'bold',
    marginLeft: 5,
  },
  body: {
    flex: 1,
    flexGrow: 1,
    padding: 5,
  }
});

export default styles;