import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Text, View, FlatList, Modal} from "react-native";
import styles from './styles';
import {addToCart, getDishes} from "../../store/actions";
import Dish from "../../components/Dish/Dish";
import Checkout from "../Checkout/Checkout";
import Button from "../../components/Button/Button";

class Pizza extends Component {
  state = {
    showCheckout: false
  };

  modalToggle = () => {
    this.setState({showCheckout: !this.state.showCheckout});
  };

  componentDidMount() {
    this.props.getDishes();
  }

  render() {
    const orderPrice = Object.keys(this.props.cart).reduce((num, dishID) => {
      const dish = this.props.dishes.find(dish => dish.id === dishID);
      if (dish) {
        num += dish.price * this.props.cart[dishID];
      }
      return num;
    }, 0);

    return (
      <Fragment>
        <Checkout
          visible={this.state.showCheckout}
          hideHandler={this.modalToggle}
        />
        <View style={styles.header}>
          <Text>Turtle Pizza</Text>
        </View>
        <View style={styles.body}>
          <FlatList
            data={this.props.dishes}
            extraData={this.props.cart}
            keyExtractor={(item) => item.id}
            // style={{height: 1}}
            contentContainerStyle={{ flexGrow: 1 }}
            style={{flexGrow: 1}}
            renderItem={({item}) => (
              <Dish
                title={item.title}
                price={item.price}
                image={item.image}
                count={this.props.cart[item.id]}
                handler={() => this.props.addToCart(item.id)}
              />
            )}
          />
        </View>
        <View style={styles.footer}>
          <Text>Order Total:</Text>
          <Text style={styles.footer_total}>{orderPrice} KGS</Text>
          <Button title="Checkout" handler={this.modalToggle} disabled={orderPrice === 0}/>
        </View>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  dishes: state.dishes,
  cart: state.cart,
});

const mapDispatchToProps = dispatch => ({
  getDishes: () => dispatch(getDishes()),
  addToCart: id => dispatch(addToCart(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Pizza);