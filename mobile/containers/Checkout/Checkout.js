import React, {Component} from 'react';
import {Modal, Text, View, ScrollView, TouchableOpacity, Image} from "react-native";
import Button from "../../components/Button/Button";
import styles from './styles';
import {connect} from "react-redux";
import {createOrder, removeFromCart} from "../../store/actions";

const DELIVERY = 150;

class Checkout extends Component {
  render() {
    const orderItems = Object.keys(this.props.cart).reduce((array, dishID) => {
      const dish = this.props.dishes.find(dish => dish.id === dishID);

      if (this.props.cart[dishID] > 0) {
        array.push(
          <View key={dishID} style={styles.orderItem}>
            <Text style={styles.description}>{dish.title} x{this.props.cart[dishID]}</Text>
            <Text style={styles.price}>{this.props.cart[dishID] * dish.price} KGS</Text>
            <TouchableOpacity
              onPress={() => this.props.removeFromCart(dishID)}
              style={styles.removeBtn}
            >
              <Image source={require('../../assets/remove.png')} style={styles.removeBtnImage} />
            </TouchableOpacity>

          </View>
        );
      }

      return array;
    }, []);

    const totalPrice = Object.keys(this.props.cart).reduce((num, dishID) => {
      const dish = this.props.dishes.find(dish => dish.id === dishID);
      if (dish) {
        num += dish.price * this.props.cart[dishID];
      }
      return num;
    }, DELIVERY);

    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.visible}
        onRequestClose={this.props.hideHandler}
      >
        <View style={styles.checkout}>
          <View style={styles.orderInfo}>
            <Text style={styles.orderHeader}>Your order:</Text>
            <ScrollView horizontal={false}>
              {orderItems}
              <View style={styles.orderPrice}>
                <View style={styles.orderPriceItem}>
                  <Text style={styles.description}>Delivery</Text>
                  <Text style={styles.price}>{DELIVERY} KGS</Text>
                </View>
                <View style={styles.orderPriceItem}>
                  <Text style={[styles.description, styles.descriptionTotal]}>Total</Text>
                  <Text style={styles.price}>{totalPrice} KGS</Text>
                </View>
              </View>
            </ScrollView>
          </View>
          <Button
            title="Cancel"
            type="Cancel"
            handler={this.props.hideHandler}
          />
          <Button
            title="Order"
            type="Continue"
            handler={() => this.props.createOrder(this.props.cart, this.props.hideHandler)}
            disabled={orderItems.length === 0}
          />
        </View>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  dishes: state.dishes,
  cart: state.cart,
});

const mapDispatchToProps = dispatch => ({
  removeFromCart: id => dispatch(removeFromCart(id)),
  createOrder: (order, callback) => dispatch(createOrder(order, callback))
});

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);
