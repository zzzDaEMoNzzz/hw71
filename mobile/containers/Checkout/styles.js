import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
  checkout: {
    flex: 1,
    padding: 10,
  },
  orderInfo: {
    flexGrow: 1,
  },
  orderHeader: {
    fontSize: 26,
    fontWeight: 'bold',
    marginBottom: 15,
    marginTop: 5,
  },
  orderPrice: {
    marginTop: 20,
  },
  orderPriceItem: {
    flexDirection: 'row',
    paddingRight: 30,
    marginBottom: 2,
  },
  description: {
    flexGrow: 1,
  },
  descriptionTotal: {
    fontWeight: 'bold',
  },
  price: {
    fontWeight: 'bold',
  },
  orderItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 2,
  },
  removeBtn: {
    padding: 3,
    width: 20,
    marginLeft: 10,
  },
  removeBtnImage: {
    width: 14,
    height: 14
  }
});

export default styles;