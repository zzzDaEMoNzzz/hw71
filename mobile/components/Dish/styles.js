import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
  dish: {
    padding: 8,
    borderColor: '#444',
    borderWidth: 1,
    marginTop: 5,
    marginBottom: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    width: 60,
    height: 60,
  },
  title: {
    flexGrow: 1,
    marginLeft: 10,
  },
  price: {
    fontWeight: 'bold',
    marginLeft: 10,
  }
});

export default styles;