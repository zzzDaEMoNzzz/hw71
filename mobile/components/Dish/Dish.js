import React, {PureComponent} from 'react';
import {TouchableOpacity, Image, Text} from "react-native";
import styles from './styles';


class Dish extends PureComponent {
  render() {
    let countElement = null;
    if (this.props.count && this.props.count > 0) {
      countElement = <Text>{this.props.count}x</Text>;
    }

    return (
      <TouchableOpacity style={styles.dish} onPress={this.props.handler}>
        <Image source={{uri: this.props.image}} style={styles.image} resizeMode='stretch'/>
        <Text style={styles.title}>{this.props.title}</Text>
        {countElement}
        <Text style={styles.price}>{this.props.price} KGS</Text>
      </TouchableOpacity>
    );
  }
}

export default Dish;