import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from "react-native";

const Button = ({title, type, handler, disabled}) => {
  return (
    <TouchableOpacity
      onPress={disabled ? null : handler}
      style={[
        styles.button,
        type === 'Cancel' ? styles.cancel : styles.continue
      ]}
      activeOpacity={disabled ? 1 : 0.5}
    >
      <Text
        style={[
          styles.text,
          disabled ? styles.disabled : null
        ]}
      >{title.toUpperCase()}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    elevation: 4,
    padding: 8,
    borderRadius: 3,
    marginTop: 5,
    backgroundColor: '#aaa',
  },
  text: {
    color: 'white',
    textAlign: 'center',
    fontWeight: '500',
    textTransform: 'uppercase',
  },
  cancel: {
    backgroundColor: '#777',
  },
  continue: {
    backgroundColor: '#f50',
  },
  disabled: {
    color: '#999'
  },
});

export default Button;
