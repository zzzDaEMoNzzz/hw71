import React from 'react';
import { StyleSheet, View } from 'react-native';
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import reducer from "./store/reducer";
import thunkMiddleware from "redux-thunk";
import Pizza from "./containers/Pizza/Pizza";

import axios from 'axios';
axios.defaults.baseURL = 'https://kurlov-hw71.firebaseio.com/';

class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Pizza/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  }
});

const store = createStore(reducer, applyMiddleware(thunkMiddleware));

const app = () => (
  <Provider store={store}>
    <App/>
  </Provider>
);

export default app;