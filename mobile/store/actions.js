import axios from "axios";

import {ADD_TO_CART, CREATE_ORDER, GET_DISHES, REMOVE_FROM_CART} from "./actionTypes";

export const getDishes = () => {
  return dispatch => {
    axios.get('dishes.json').then(response => {
      const dishes = Object.keys(response.data).map(id => ({...response.data[id], id}));
      dispatch({type: GET_DISHES, dishes});
    });
  };
};

export const addToCart = id => ({type: ADD_TO_CART, id});
export const removeFromCart = id => ({type: REMOVE_FROM_CART, id});

export const createOrder = (order, callback) => {
  return dispatch => {
    axios.post('orders.json', order).then(() => {
      dispatch({type: CREATE_ORDER});
      if (callback) callback();
    });
  };
};