import {ADD_TO_CART, CREATE_ORDER, GET_DISHES, REMOVE_FROM_CART} from "./actionTypes";

const initialState = {
  dishes: [],
  cart: {},
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DISHES:
      return {...state, dishes: action.dishes};
    case ADD_TO_CART:
      const currentCount = state.cart[action.id] || 0;
      return {
        ...state,
        cart: {
          ...state.cart,
          [action.id]: currentCount + 1
        }
      };
    case REMOVE_FROM_CART:
      let removeCount = state.cart[action.id];
      if (removeCount > 0) {
        removeCount--;
      }
      return {
        ...state,
        cart: {
          ...state.cart,
          [action.id]: removeCount
        }
      };
    case CREATE_ORDER:
      return {...state, cart: {}};
    default:
      return state;
  }
};

export default reducer;