export const GET_DISHES = 'GET_DISHES';
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const CREATE_ORDER = 'CREATE_ORDER';