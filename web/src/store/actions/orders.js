import axios from "axios";
import {GET_ORDERS_FAILURE, GET_ORDERS_REQUEST, GET_ORDERS_SUCCESS} from "./actionTypes";

export const getOrdersRequest = () => ({type: GET_ORDERS_REQUEST});
export const getOrdersSuccess = orders => ({type: GET_ORDERS_SUCCESS, orders});
export const getOrdersFailure = () => ({type: GET_ORDERS_FAILURE});

export const getOrders = () => {
  return dispatch => {
    dispatch(getOrdersRequest());

    axios.get('orders.json').then(
      response => {
        if (response.data) {
          const orders = Object.keys(response.data).map(id => ({id, order: {...response.data[id]}}));

          dispatch(getOrdersSuccess(orders));
        }
      },
      error => dispatch(getOrdersFailure())
    );
  };
};

export const deleteOrder = id => {
  return dispatch => {
    axios.delete(`orders/${id}.json`).then(() => dispatch(getOrders()));
  }
};