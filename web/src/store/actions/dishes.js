import axios from 'axios';
import {
  ADD_DISH_FAILURE,
  ADD_DISH_REQUEST,
  ADD_DISH_SUCCESS,
  ADDING_FORM_INPUT_CHANGE, EDIT_DISH, EDIT_DISH_CANCEL, GET_DISHES_FAILURE,
  GET_DISHES_REQUEST, GET_DISHES_SUCCESS
} from "./actionTypes";

export const addDishRequest = () => ({type: ADD_DISH_REQUEST});
export const addDishSuccess = () => ({type: ADD_DISH_SUCCESS});
export const addDishFailure = () => ({type: ADD_DISH_FAILURE});

export const addDish = (data, callback) => {
  return dispatch => {
    dispatch(addDishRequest());

    axios.post('dishes.json', data).then(
      response => dispatch(addDishSuccess()),
      error => dispatch(addDishFailure(error))
    ).finally(() => {
      if (callback) callback();
      dispatch(getDishes());
    });
  };
};

export const addingFormInputChange = (event, stateTitle) => ({
  type: ADDING_FORM_INPUT_CHANGE,
  value: event.target.value,
  stateTitle
});

export const getDishesRequest = () => ({type: GET_DISHES_REQUEST});
export const getDishesSuccess = dishes => ({type: GET_DISHES_SUCCESS, dishes});
export const getDishesFailure = () => ({type: GET_DISHES_FAILURE});

export const getDishes = () => {
  return dispatch => {
    dispatch(getDishesRequest());

    axios.get('dishes.json').then(
      response => {
        if (response.data) {
          const dishes = Object.keys(response.data).map(id => ({...response.data[id], id}));

          dispatch(getDishesSuccess(dishes));
        }
      },
      error => dispatch(getDishesFailure())
    );
  };
};

export const deleteDish = id => {
  return dispatch => {
    if (window.confirm('Are you sure?')) {
      axios.delete(`dishes/${id}.json`).then(() => {
        dispatch(getDishes());
      });
    }
  };
};

export const editDish = id => ({type: EDIT_DISH, id});
export const editCancel = () => ({type: EDIT_DISH_CANCEL});

export const saveDish = (id, data, callback) => {
  return dispatch => {
    dispatch(addDishRequest());

    axios.put(`dishes/${id}.json`, data).then(
      response => dispatch(addDishSuccess()),
      error => dispatch(addDishFailure(error))
    ).finally(() => {
      if (callback) callback();
      dispatch(getDishes());
    });
  };
};