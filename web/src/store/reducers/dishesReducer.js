import {
  ADD_DISH_FAILURE,
  ADD_DISH_REQUEST,
  ADD_DISH_SUCCESS,
  ADDING_FORM_INPUT_CHANGE, EDIT_DISH, EDIT_DISH_CANCEL, GET_DISHES_FAILURE,
  GET_DISHES_REQUEST, GET_DISHES_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  list: [],
  addingDish: false,
  loadingDishes: true,
  addingFormTitle: '',
  addingFormPrice: '',
  addingFormImage: '',
  editModeID: null,
};

const dishesReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_DISH_REQUEST:
      return {...state, addingDish: true};
    case ADD_DISH_SUCCESS:
    case ADD_DISH_FAILURE:
      return {
        ...state,
        addingDish: false,
        editModeID: null,
        addingFormTitle: '',
        addingFormPrice: '',
        addingFormImage: '',
      };
    case ADDING_FORM_INPUT_CHANGE:
      return {...state, [action.stateTitle]: action.value};
    case GET_DISHES_REQUEST:
      return {...state, loadingDishes: true};
    case GET_DISHES_SUCCESS:
    case GET_DISHES_FAILURE:
      return {...state, list: action.dishes, loadingDishes: false};
    case EDIT_DISH:
      const dishInfo = state.list.find((element) => element.id === action.id);

      if (dishInfo) {
        return {
          ...state,
          editModeID: action.id,
          addingFormTitle: dishInfo.title,
          addingFormPrice: dishInfo.price,
          addingFormImage: dishInfo.image,
        };
      }
      return {...state};
    case EDIT_DISH_CANCEL:
      return {
        ...state,
        editModeID: null,
        addingFormTitle: '',
        addingFormPrice: '',
        addingFormImage: '',
      };
    default:
      return state;
  }
};

export default dishesReducer;