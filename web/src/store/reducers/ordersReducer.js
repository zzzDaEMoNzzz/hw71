import {GET_ORDERS_FAILURE, GET_ORDERS_REQUEST, GET_ORDERS_SUCCESS} from "../actions/actionTypes";

const initialState = {
  list: [],
  loading: true,
};

const ordersReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ORDERS_REQUEST:
      return {...state, loading: true};
    case GET_ORDERS_SUCCESS:
    case GET_ORDERS_FAILURE:
      return {...state, list: action.orders, loading: false};
    default:
      return state;
  }
};

export default ordersReducer;