import React, {Component} from 'react';
import './Dishes.css';
import {connect} from 'react-redux';
import {deleteDish, editCancel, editDish, getDishes} from "../../store/actions/dishes";
import AddingForm from "./AddingForm/AddingForm";
import Dish from "./Dish/Dish";
import Preloader from "../../components/Preloader/Preloader";

class Dishes extends Component {
  state = {
    showAddingForm: false,
  };

  toggleAddingForm = () => {
    if (this.state.showAddingForm) this.props.editCancel();

    this.setState({
      showAddingForm: !this.state.showAddingForm
    });
  };

  componentDidMount() {
    this.props.getDishes();
  }

  render() {
    const dishesList = this.props.dishes.map(dish => (
      <Dish
        key={dish.id}
        title={dish.title}
        price={dish.price}
        image={dish.image}
        deleteDish={() => this.props.deleteDish(dish.id)}
        editDish={() => {
          this.props.editDish(dish.id);
          this.setState({showAddingForm: true});
        }}
      />
    ));

    return (
      <div className="Dishes">
        <div className="Dishes-header">
          <h2>Dishes</h2>
          <button onClick={this.toggleAddingForm}>{this.state.showAddingForm ? "Cancel" : "Add new Dish"}</button>
        </div>
        <AddingForm
          show={this.state.showAddingForm}
          toggleHandler={this.toggleAddingForm}
        />
        {this.props.loading ? <Preloader/> : dishesList}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  dishes: state.dishes.list,
  loading: state.dishes.loadingDishes
});

const mapDispatchToProps = dispatch => ({
  getDishes: () => dispatch(getDishes()),
  deleteDish: id => dispatch(deleteDish(id)),
  editDish: id => dispatch(editDish(id)),
  editCancel: () => dispatch(editCancel()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Dishes);