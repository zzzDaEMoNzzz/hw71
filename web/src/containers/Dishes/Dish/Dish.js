import React from 'react';
import './Dish.css';

const Dish = ({title, price, image, deleteDish, editDish}) => {
  return (
    <div className="Dish">
      <img src={image} alt=""/>
      <span className="Dish-title">{title}</span>
      <span className="Dish-price">{price} KGS</span>
      <button onClick={editDish}>Edit</button>
      <button onClick={deleteDish}>Delete</button>
    </div>
  );
};

export default Dish;
