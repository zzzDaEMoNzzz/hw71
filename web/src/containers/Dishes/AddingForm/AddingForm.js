import React, {Component} from 'react';
import {addDish, addingFormInputChange, saveDish} from "../../../store/actions/dishes";
import {connect} from "react-redux";
import './AddingForm.css';

class AddingForm extends Component {
  render() {
    const formClasses = ['AddingForm'];
    if (this.props.show) formClasses.push('active');

    const addHandler = event => {
      event.preventDefault();

      this.props.addDish({
        title: this.props.title,
        price: this.props.price,
        image: this.props.image
      }, this.props.toggleHandler);
    };

    const saveHandler = event => {
      event.preventDefault();

      this.props.saveDish(this.props.editModeID, {
        title: this.props.title,
        price: this.props.price,
        image: this.props.image
      }, this.props.toggleHandler);
    };

    return (
      <form
        className={formClasses.join(' ')}
        onSubmit={this.props.editModeID ? saveHandler : addHandler}
      >
        <label>
          Title
          <input
            type="text"
            value={this.props.title}
            onChange={event => this.props.onInputChange(event, 'addingFormTitle')}
            disabled={this.props.adding}
          />
        </label>
        <label>
          Price
          <input
            type="text"
            value={this.props.price}
            onChange={event => this.props.onInputChange(event, 'addingFormPrice')}
            disabled={this.props.adding}
          />
        </label>
        <label>
          Image
          <input
            type="text"
            value={this.props.image}
            onChange={event => this.props.onInputChange(event, 'addingFormImage')}
            disabled={this.props.adding}
          />
        </label>
        <button
          className={this.props.adding ? "adding" : ""}
          disabled={this.props.adding}
        >
          {this.props.editModeID ? "Save" : "Add"}
        </button>
      </form>
    );
  }
}

const mapStateToProps = state => ({
  adding: state.dishes.addingDish,
  title: state.dishes.addingFormTitle,
  price: state.dishes.addingFormPrice,
  image: state.dishes.addingFormImage,
  editModeID: state.dishes.editModeID,
});

const mapDispatchToProps = dispatch => ({
  addDish: (data, callback) => dispatch(addDish(data, callback)),
  saveDish: (id, data, callback) => dispatch(saveDish(id, data, callback)),
  onInputChange: (event, stateTitle) => dispatch(addingFormInputChange(event, stateTitle)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddingForm);