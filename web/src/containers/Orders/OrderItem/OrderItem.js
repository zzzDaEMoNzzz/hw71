import React from 'react';
import './OrderItem.css';

const DELIVERY_PRICE = 150;

const OrderItem = ({data, handler}) => {
  const orderDishes = data.map((dish, index) => (
    <div key={dish + index} className="OrderItem_DishItem">
      <span>{dish.count}x</span>
      <span className="OrderItem_DishItemTitle">{dish.title}</span>
      <span><strong>{dish.price * dish.count} KGS</strong></span>
    </div>
  ));

  const orderPrice = data.reduce((price, dish) => {
    price += parseInt(dish.price * dish.count);

    return price;
  }, 0);

  return (
    <div className="OrderItem">
      <div>{orderDishes}</div>
      <div className="OrderItemDelivery">
        <span>Delivery</span>
        <span>{DELIVERY_PRICE} KGS</span>
      </div>
      <div className="OrderItemTotal">
        <span>Total</span>
        <span>{orderPrice + DELIVERY_PRICE} KGS</span>
      </div>
      <button onClick={handler}>Complete order</button>
    </div>
  );
};

export default OrderItem;
