import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteOrder, getOrders} from "../../store/actions/orders";
import {getDishes} from "../../store/actions/dishes";
import OrderItem from "./OrderItem/OrderItem";
import './Orders.css';
import Preloader from "../../components/Preloader/Preloader";

class Orders extends Component {
  componentDidMount() {
    if (this.props.dishes.length === 0) {
      this.props.getDishes();
    }

    this.props.getOrders();
  }

  render() {
    let ordersList = <p>There are currently no orders</p>;

    if (this.props.orders.length !== 0 && this.props.dishes.length !== 0) {
      ordersList = [...this.props.orders].reverse().map((orderDishes, index) => {
        const orderData = Object.keys(orderDishes.order).map(dishID => {
          const dish = this.props.dishes.find(dish => dish.id === dishID);
          const orderCount = orderDishes.order[dishID];

          return {title: dish.title, price: dish.price, count: orderCount};
        });

        return <OrderItem key={index} data={orderData} handler={() => this.props.deleteOrder(orderDishes.id)}/>;
      });
    }

    return (
      <div className="Orders">
        <h2>Orders</h2>
        {this.props.loading ? <Preloader/> : ordersList}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  dishes: state.dishes.list,
  orders: state.orders.list,
  loading: state.orders.loading,
});

const mapDispatchToProps = dispatch => ({
  getOrders: () => dispatch(getOrders()),
  getDishes: () => dispatch(getDishes()),
  deleteOrder: id => dispatch(deleteOrder(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Orders);