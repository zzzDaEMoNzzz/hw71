import React from 'react';
import './Preloader.css';

const Preloader = () => <div className="Preloader"/>;

export default Preloader;
