import React from 'react';
import {NavLink, Link, withRouter} from "react-router-dom";
import './Header.css';

const Header = props => {
  const isDishes = () => {
    return props.location.pathname === '/' || props.location.pathname === '/dishes';
  };

  return (
    <header className="Header">
      <Link to="/">Turtle Pizza Admin</Link>
      <nav>
        <NavLink to="/dishes" isActive={isDishes}>Dishes</NavLink>
        <NavLink to="/orders">Orders</NavLink>
      </nav>
    </header>
  );
};

export default withRouter(Header);
