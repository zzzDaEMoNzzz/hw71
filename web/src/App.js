import React, { Component } from 'react';
import './App.css';
import {Switch, Route} from "react-router-dom";
import Dishes from "./containers/Dishes/Dishes";
import Orders from "./containers/Orders/Orders";
import Layout from "./components/Layout/Layout";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Layout>
          <Switch>
            <Route path="/" exact component={Dishes}/>
            <Route path="/dishes" component={Dishes}/>
            <Route path="/orders" component={Orders}/>
          </Switch>
        </Layout>
      </div>
    );
  }
}

export default App;
